<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Register</title>
</head>
<body>
    <form action="../model/insert.php" method="POST">
        <label for="firstname">First Name: </label>
        <input type="text" id="firstname" name="firstname" />
        <br>
        <br>
        <label for="lastname">Last Name: </label>
        <input type="text" id="lastname" name="lastname" />
        <br>
        <br>
        <label for="email">Email: </label>
        <input type="email" id="email" name="email" />
        <br>
        <br>
        <label for="username">Username: </label>
        <input type="text" id="username" name="username" />
        <br>
        <br>
        <label for="password">Password: </label>
        <input type="password" id="password" name="password" />
        <br>
        <br>
        <label for="role">Role: </label>
        <select name="role" id="role">
            <option value="admin">Admin</option>
            <option value="staff">Staff</option>
        </select>
        <br>
        <br>
        <label for="schedule">Schedule: </label>
        <input type="text" id="schedule" name="schedule" />
        <br>
        <br>
        <input type="submit" id="btn" value="Login" />
    </form>
</body>
</html>