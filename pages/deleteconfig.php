<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "interactive_map";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT user_id, first_name, last_name FROM users";
$result = $conn->query($sql);

$conn->close();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <table>
    <tr>
    <th>User ID</th>
    <th>First Name</th>
    <th>Last Name</th>
    </tr>
    <?php
if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        echo "<tr><td>" . $row["user_id"]. "</td><td> ". $row["first_name"]. "</td><td>". $row["last_name"]."</td></tr>";
    }
} else {
    echo "0 results";
} 
    ?>
     
    </table>
</body>
</html>